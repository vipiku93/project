const express=require('express');
const router=express.Router();
const Trade=require('../models/trade');
var HashMap = require('hashmap');
var request = require('sync-request');


//api for returning the entire portfolio with trades
router.get('',function(req,res){
	Trade.find({}).then(function(result){
	res.send(result);
	});
});


//api for returning the holding of the portfolio with details
router.get('/holdings',function(req,res){
	
	Trade.find({}).then(function(result){

		var map = new HashMap();

		for ( var i = 0; i < result.length; i++ ) {
			if(result[i].buy==true){

				if(map.has(result[i].stock)==true){
					//calculating the avg of stock and setting it in map again
					var tempStock=map.get(result[i].stock);
					var tempValue=tempStock.quantity*tempStock.price;
					var oldValue=result[i].price*result[i].quantity;
					var newValue=oldValue+tempValue;
					var totalquantity=tempStock.quantity+result[i].quantity;
					var avgprice=newValue/totalquantity;
					var object={price:avgprice,quantity:totalquantity};
					map.set(result[i].stock,object);
				}
				else{
					var object={price:result[i].price,quantity:result[i].quantity};
					map.set(result[i].stock, object);
				}
			}
			
		}
		res.send(map);
	});

});


function createMapStock(result,index,map){

	if(index==result.length){
			return map;
		}

	var stockName=result[index].stock;

	//Google finance api for getting the last price of the stock
	var url='https://finance.google.com/finance?q=NSE:'+stockName+'&output=json';

	var i=index;
	if(result[i].buy==false)
	{	
		//for just excluding the selling trade
		map=createMapStock(result,index+1,map);
	}
	else
	{
		if(map.has(result[i].stock)==true){
			//calculating avg price and setting the map again
			var temp=map.get(result[i].stock);
		  	var price=result[i].price;
			var quantity=result[i].quantity;
		  	var newValue=price*quantity;
		  	var oldValue=temp.price*temp.quantity;
		  	var totalquantity=quantity+temp.quantity;
		  	var avgPrice=(oldValue+newValue)/totalquantity;  		
			var object={price:avgPrice,quantity:totalquantity,close:temp.close};
			map.set(result[i].stock, object);
			map=createMapStock(result,index+1,map);
	}
	else{
			var res = request('GET', url);
			var body=res.body.toString('utf-8');
			body=body.substring(5,body.length-2);
			
			if(body.charAt(1)!='{')
				return null;

		  	let json = JSON.parse(body);
		  	var lastclose=json.l;
		  	var tempprice=result[i].price;
		  	var tempquantity=result[i].quantity;
		  	var object={price:tempprice,quantity:tempquantity,close:lastclose};
			map.set(result[i].stock, object);
	  		map=createMapStock(result,index+1,map);
	}
	}
	
	return map;
}


//api for calculating the cummulative returns of the portfolio
router.get('/returns',function(req,res){

	Trade.find({}).then(function(result){
		
		var map = new HashMap();
		map=createMapStock(result,0,map);
		if(map==null)
			return res.send({"error":"stock name error with google finance api"});
		
		var initialinvestment=0;
		var currentinvestment=0;

		//calculating the initalvalue and current value
		map.forEach(function(value, key) {	
	    var temp=value;
	    initialinvestment+=(value.quantity*value.price);
	    var close=(parseFloat(value.close.replace(',','')));
	    currentinvestment+=(value.quantity*close);
		});

		//total cummulative returns of the portfolio
		var returns=100*(currentinvestment-initialinvestment)/initialinvestment;

		res.send({"returns(%)":returns,"currentvalue":currentinvestment,"invested":initialinvestment,"holdings":map});
	});
});


//api for adding trade
router.post('/addTrade',function(req,res,next){
	
	Trade.create(req.body).then(function(trade){
		res.send(trade);
	}).catch(next);

});

//api for getting the trade details by id
router.get('/detailsTrade/:id',function(req,res,next){
	Trade.findById({_id:req.params.id}).then(function(result){
		res.send(result);
	});
});

//api for deleting trade by id
router.delete('/removeTrade/:id',function(req,res,next){
	Trade.findByIdAndRemove({_id:req.params.id}).then(function(result){
		res.send(result);
	});
});

//api for modifying trade details by id
router.put('/updateTrade/:id',function(req,res,next){
	Trade.findByIdAndUpdate({_id:req.params.id},req.body).then(function(result){
		Trade.findById({_id:req.params.id}).then(function(result){
			res.send(result);
		})
	});
});


module.exports=router;