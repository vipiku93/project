const assert=require('assert');
const Trade=require('../models/trade');


//describe test
describe('Saving trade details',function(){

	//creating trade
	it('save trade details to mongodb',function(done){

		var trade=new Trade({
		stock:"ACC",
		price:250,
		quantity:500,
		buy:"buy",
		date:"12/3/2017"
		});

		trade.save().then(function(){
			assert(trade.isNew===false);
			done();
		});
	});
});