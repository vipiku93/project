const mongoose=require('mongoose');

//connect to db
mongoose.Promise=global.Promise;



before(function(done){
//connect to mongodb
//mongoose.connect('mongodb://admin:admin@ds135594.mlab.com:35594/smallcase');
mongoose.connect('mongodb://localhost/trade');

mongoose.connection.once('open',function(){
console.log("connection has been made");
done();
}).on('error',function(error){
console.log('connection error: '+error);
});

});


//drop the characters collection before each test
beforeEach(function(done){

	//drop the collections
	mongoose.connection.collections.trades.drop(function(){
		done();
	});
});