const assert=require('assert');
const Trade=require('../models/trade');


//describe test
describe('testing trade details',function(){

	var trade;
	
	beforeEach(function(done){

		 trade=new Trade({
		stock:"ACC",
		price:250,
		quantity:500,
		buy:"buy",
		date:"12/3/2017"
		});


		trade.save().then(function(){
			//assert(trade.isNew===false);
			done();
		});

	});

	//finding trade detail testing
	it('find trade details from mongodb',function(done){

		Trade.findOne({stock:'ACC'}).then(function(result){
			assert(result.stock==='ACC');
			done();
		});
	});

	//finding trade detail by id testing
	it('find one trade details by id from mongodb',function(done){

		Trade.findOne({_id:trade._id}).then(function(result){
			assert(result._id.toString()===trade._id.toString());
			done();
		});
	});

	//updating trade detail testing
	it('updating trade details from mongodb',function(done){

		Trade.findOneAndUpdate({stock:'ACC'},{stock:'TITAN'}).then(function(){
				
				Trade.findOne({_id:trade._id}).then(function(result){
					assert(result.stock==='TITAN');
					done();
				});
		});
	});

	//delete trade detail testing
	it('deleting trade details from mongodb',function(done){

		Trade.findOneAndRemove({stock:'ACC'}).then(function(){
				
				Trade.findOne({stock:'ACC'}).then(function(result){
					assert(result===null);
					done();
				});
		});
	});
});