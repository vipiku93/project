const mongoose=require('mongoose');
const Schema=mongoose.Schema;


//create schema and model
const TradeSchema= new Schema({
	stock:{
		type:String,
		required:[true,'Name field is required']
	},
	price:{
		type:Number,
		required:[true,'price field is required']
	},
	quantity:{
		type:Number,
		required:[true,'quantity field is required']
	},
	buy:{
		type:Boolean,
		required:[true,'buy field is required']
	},
	date:{
		type: Date,
	}
});

const Trade=mongoose.model('trade',TradeSchema);


module.exports=Trade;