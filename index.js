const express=require('express');
const bodyParser=require('body-parser');
const routes= require('./routes/api');
const mongoose=require('mongoose');


//setup
const app =express();

//promise
mongoose.Promise=global.Promise;

//connect to mongodb
//mongoose.connect('mongodb://admin:admin@ds135594.mlab.com:35594/smallcase');
mongoose.connect('mongodb://localhost/trade');


app.use(bodyParser.json());

app.use('/portfolio',routes);

//error handling
app.use(function(err,req,res,next){
	console.log("error   "+err);
	res.status(422).send({error:err.message});
})


//listen for request 
app.listen(process.env.PORT||4000,function(){
	console.log('server is running');
});